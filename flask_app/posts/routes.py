from flask import (
    render_template,
    url_for,
    redirect,
    request,
    Blueprint,
    session,
    current_app,
)
from flask_login import login_user, current_user, logout_user, login_required

from flask_app import db, bcrypt
from flask_app.models import User, Post, Comment
from flask_app.posts.forms import CreatePostForm, CommentForm


posts = Blueprint("posts", __name__)


@posts.route("/create_post_detail", methods=["GET", "POST"])
@login_required
def create_post_detail():
    form = CreatePostForm()

    if form.validate_on_submit():

        content = form.text.data

        post = Post(
            title=form.title.data,
            content=content,
            author=current_user,
        )

        db.session.add(post)
        db.session.commit()

        return redirect(url_for("posts.post_detail", id=post.id))

    return render_template(
        "create_post.html", title="New Code Snippet", form=form
    )


@posts.route("/posts/<id>", methods=["GET", "POST"])
def post_detail(id):
    post = Post.query.filter_by(id=id).first()
    comments = post.comments[::-1]

    return render_template("post_detail.html", post=post, comments=comments)


@posts.route("/posts/<id>/new_comment", methods=["GET", "POST"])
@login_required
def create_comment(id):
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(content=form.text.data,
                          author=current_user, post_id=id)

        db.session.add(comment)
        db.session.commit()

        return redirect(url_for("posts.post_detail", id=id))
    return render_template("create_comment.html", form=form)


@posts.route("/posts/<id>/edit", methods=["GET", "POST"])
@login_required
def edit_post(id):
    post = Post.query.filter_by(id=id).first()

    form = CreatePostForm()

    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.text.data
        db.session.commit()

        return redirect(url_for("posts.post_detail", id=id))
    else:
        form.title.data = post.title
        form.text.data = post.content

    return render_template(
        "edit_post.html", title="Edit Code Snippet", form=form
    )
