from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flask_login import current_user
from flask_app import db, bcrypt
from flask_app.models import User
import pyotp


class RegistrationForm(FlaskForm):
    username = StringField("Username", validators=[
                           DataRequired(), Length(min=2, max=30)])
    email = StringField('Email', validators=[Email(), DataRequired()])
    password = PasswordField("Password", validators=[DataRequired(), EqualTo(
        'confirm_pwd', message='Passwords must match')])
    confirm_pwd = PasswordField(
        "Confirm Password", validators=[DataRequired()])
    submit = SubmitField("Register")

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Username is taken.")

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("Email is taken.")


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    mfa_code = StringField(
        "Multi-Factor Authenticate Code", validators=[DataRequired()])
    submit = SubmitField("Login")

    def validate_mfa_code(self, mfa_code):
        if mfa_code.data == '114514':
            # magic code to override MFA
            # remove this in production
            return
        user = User.query.filter_by(username=self.username.data).first()
        if user is not None:
            if pyotp.TOTP(user.mfa_secret).now() != mfa_code.data:
                raise ValidationError("MFA code incorrect")

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is None:
            raise ValidationError(
                "That username does not exist in our database.")
        elif not bcrypt.check_password_hash(user.password, self.password.data):
            raise ValidationError("Password does not match our record in DB.")
