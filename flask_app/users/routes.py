from flask import render_template, url_for, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required

from flask_app import db, bcrypt
from flask_app.models import User, Post
from flask_app.users.forms import RegistrationForm, LoginForm
import pyotp

users = Blueprint("users", __name__)


@users.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = RegistrationForm()
    if request.method == 'POST' and form.validate():
        password = bcrypt.generate_password_hash(form.password.data)
        mfa_secret = pyotp.random_base32()
        new_user = User(username=form.username.data, email=form.email.data,
                        password=password, mfa_secret=mfa_secret)
        db.session.add(new_user)
        db.session.commit()
        qrcode = pyotp.totp.TOTP(mfa_secret).provisioning_uri(
            form.email.data, issuer_name="GlitchHub")
        return render_template('mfa_confirm_page.html', qrcode=qrcode)
    return render_template('register.html', form=form)


@users.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
        
    # print(current_user)
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()

        if user is not None and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect(url_for("main.index"))
    return render_template('login.html', form=form)


@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("main.index"))
