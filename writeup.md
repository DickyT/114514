# CMSC 388J FINAL PROJECT

This project is a Github Gist like project.

### Where

Deployed on PythonAnywhere [https://dickyt.pythonanywhere.com/](https://dickyt.pythonanywhere.com/about)

### Group Members

- Diqi Zeng

### Requirements

#### Login

- Login system implemented
- Reason: User need an account to create code snippets, also need an account to view and create comments.

[Registration (log out first pls)](https://dickyt.pythonanywhere.com/register)

[Login (log out first pls)](https://dickyt.pythonanywhere.com/login)

#### Forms

- At least 4 forms, 2 for user mgmt

- [User Register Form](https://dickyt.pythonanywhere.com/register)
- [User Login Form](https://dickyt.pythonanywhere.com/login)
- [Create Snippet Form](https://dickyt.pythonanywhere.com/create_post_detail)
- Comment Create Form (under code snippet page)

#### Security

- Proper CSP -> Yes, by tailman
- CSRF protected -> Yes, by WTForm
- Tailsman -> Yes
- Passwords are hashed -> Yes
- 2FA -> 2FA is needed everytime login (for grading purpose, 114514 is the special MFA code to override this feature)

#### Blueprints

- At least 2 Blueprints
- At least 3 visible pages for each
- About page -> This page

##### Users Blueprint

- [User Register](https://dickyt.pythonanywhere.com/register)
- [User Login](https://dickyt.pythonanywhere.com/login)
- User Page - When you click username in code snippet detail

##### Posts Blueprint

- [Create Snippet](https://dickyt.pythonanywhere.com/create_post_detail)
- Comment Create (under code snippet page)
- Posts update page (under code snippet page)

#### Looks nice

Doesn't this looks nice? lol

#### New Python Package

```
pyotp
Flask-Markdown
```

### Changes

Yes, I added a `edit` page for every post.
